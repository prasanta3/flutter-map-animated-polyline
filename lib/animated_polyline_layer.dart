import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong2/latlong.dart';

class AnimatedPolylineLayer extends StatefulWidget {
  final List<LatLng> points;
  final Duration? animationDuration;
  final Color color;

  const AnimatedPolylineLayer({
    super.key,
    required this.points,
    this.animationDuration,
    this.color = Colors.green,
  });

  @override
  State<AnimatedPolylineLayer> createState() => _AnimatedPolylineLayerState();
}

class _AnimatedPolylineLayerState extends State<AnimatedPolylineLayer>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> animation;
  late Polyline polylineOpt;
  double progress = 0.0;

  @override
  void initState() {
    _controller = AnimationController(
      duration: widget.animationDuration ?? const Duration(milliseconds: 3000),
      vsync: this,
    );

    animation = Tween<double>(begin: 0.0, end: 1.0).animate(_controller)
      ..addListener(() {
        setState(() {
          progress = _controller.value;
        });
      });

    animation.addStatusListener((status) {
      if (status != AnimationStatus.completed) {
        // _controller.reset();
        _controller.forward();
      } else {
        _controller.reset();
      }
    });
    _controller.forward();
    super.initState();
    polylineOpt = Polyline(
      points: widget.points,
      strokeWidth: 4,
      color: widget.color,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final map = FlutterMapState.maybeOf(context)!;
    // print("Value: _$_value");
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints bc) {
        final size = Size(bc.maxWidth, bc.maxHeight);
        polylineOpt.offsets.clear();
        _fillOffsets(polylineOpt.offsets, polylineOpt.points, map);

        return Stack(
          children: [
            AnimatedBuilder(
              animation: _controller,
              builder: (context, widget) {
                return CustomPaint(
                  painter: PolylinePainter(
                    polylineOpt: polylineOpt,
                    map: map,
                    progress: progress,
                  ),
                  size: size,
                );
              },
            ),
          ],
        );
      },
    );
  }

  void _fillOffsets(
    final List<Offset> offsets,
    final List<LatLng> points,
    FlutterMapState map,
  ) {
    final len = points.length;
    for (var i = 0; i < len; ++i) {
      final point = points[i];
      final offset = map.getOffsetFromOrigin(point);
      offsets.add(offset);
    }
  }
}

class PolylinePainter extends CustomPainter {
  final Polyline polylineOpt;
  final double progress;

  final FlutterMapState map;

  PolylinePainter(
      {required this.polylineOpt, required this.map, required this.progress});

  @override
  void paint(Canvas canvas, Size size) {
    if (polylineOpt.offsets.isEmpty) {
      return;
    }

    final rect = Offset.zero & size;
    canvas.clipRect(rect);

    late final double strokeWidth;

    if (polylineOpt.useStrokeWidthInMeter) {
      final firstPoint = polylineOpt.points.first;
      final firstOffset = polylineOpt.offsets.first;
      final r = const Distance().offset(
        firstPoint,
        polylineOpt.strokeWidth,
        180,
      );
      final delta = firstOffset - map.getOffsetFromOrigin(r);

      strokeWidth = delta.distance;
    } else {
      strokeWidth = polylineOpt.strokeWidth;
    }
    final paint = Paint()
      ..strokeWidth = strokeWidth
      ..strokeCap = polylineOpt.strokeCap
      ..strokeJoin = polylineOpt.strokeJoin
      ..blendMode = BlendMode.srcOver
      ..color = polylineOpt.color;

    final radius = paint.strokeWidth / 2;
    ui.Path path = _createLine(polylineOpt.offsets, radius);

    ui.PathMetrics pathMetrics = path.computeMetrics(); 
    for (ui.PathMetric pathMetric in pathMetrics) {
      ui.Path extractPath =
          pathMetric.extractPath(0.0, pathMetric.length * progress);
      canvas.drawPath(extractPath, paint);
    }
  }

  ui.Path _createLine(List<Offset> offsets, radius) {
    final path = ui.Path();
    for (var i = 0; i < offsets.length - 1; i++) {
      path.addOval(Rect.fromCircle(
          center: Offset(offsets[i].dx, offsets[i].dy), radius: radius));
    }
    return path;
  }

  @override
  bool shouldRepaint(PolylinePainter oldDelegate) => false;
}

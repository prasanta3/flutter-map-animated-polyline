import 'package:latlong2/latlong.dart';

class Constants {
  const Constants._();
  static LatLng defaultPosition = LatLng(23.738793, 91.268843);
  static String tileTemplateUrl =
      'https://tile.openstreetmap.org/{z}/{x}/{y}.png';
  static List<LatLng> points = [
    LatLng(23.738793, 91.268843),
    LatLng(23.738793, 91.268843),
    LatLng(23.739102, 91.269011),
    LatLng(23.739500, 91.269096),
    LatLng(23.739813, 91.269199),
    LatLng(23.740062, 91.268480),
    LatLng(23.740067, 91.267891),
    LatLng(23.739966, 91.267349),
    LatLng(23.739531, 91.267260),
    LatLng(23.739104, 91.267186),
    LatLng(23.739104, 91.267186),
    LatLng(23.738754, 91.267266),
  ];
}

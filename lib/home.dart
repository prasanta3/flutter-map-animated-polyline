

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'animated_polyline_layer.dart';
import 'constants.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FlutterMap(
        options: _mapOption(),
        // children: [MyHomePage()],
        children: [
          TileLayer(
            urlTemplate: Constants.tileTemplateUrl,
            userAgentPackageName: 'com.example.path_tracing',
          ),
          AnimatedPolylineLayer(
            points: Constants.points,
          )
        ],
      ),
    );
  }

  MapOptions _mapOption() {
    return MapOptions(
        center: Constants.defaultPosition,
        zoom: 15,
        maxZoom: 15,
        minZoom: 3,
      );
  }
}
